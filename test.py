import unittest
import my_sum


class TestCalc(unittest.TestCase):
    def test_add(self):
        result = my_sum.add(5, 8)
        self.assertEqual(result, 13)

    def test_sub(self):
        result = my_sum.sub(8, 4)
        self.assertEqual(result, 4)

    def test_mult(self):
        result = my_sum.mult(8, 2)
        self.assertEqual(result, 16)

    def test_div(self):
        result = my_sum.div(10, 2)
        self.assertEqual(result, 5)
        with self.assertRaises(ValueError):
            my_sum.div(10, 0)


if __name__ == "__main__":
    unittest.main()
